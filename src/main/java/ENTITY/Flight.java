package ENTITY;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Time;
import java.util.List;

@Entity
public class Flight {

    @Id
    int flightNo;
    String source;
    String destination;
    Time takeoff;
    Time landing;
    int price;

    @OneToMany(mappedBy = "flight")
    List<Booking> bookingList;

    public Flight(int flightNo, String source, String destination, Time takeoff, Time landing, int price) {
        this.flightNo = flightNo;
        this.source = source;
        this.destination = destination;
        this.takeoff = takeoff;
        this.landing = landing;
        this.price = price;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Time getTakeoff() {
        return takeoff;
    }

    public void setTakeoff(Time takeoff) {
        this.takeoff = takeoff;
    }

    public Time getLanding() {
        return landing;
    }

    public void setLanding(Time landing) {
        this.landing = landing;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    public Flight() {
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightNo=" + flightNo +
                ", source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", takeoff=" + takeoff +
                ", landing=" + landing +
                ", price=" + price +
                '}';
    }
}
