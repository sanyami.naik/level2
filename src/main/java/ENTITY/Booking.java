package ENTITY;


import javax.persistence.*;

@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int bookingId;

    @ManyToOne
    User user;

    @ManyToOne
    Flight flight;

}
