package DAO;

import ENTITY.User;

import javax.persistence.EntityManager;

public class UserImplementation {

    public void insertUser(User user)
    {
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
