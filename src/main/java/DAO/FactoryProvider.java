package DAO;

import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FactoryProvider {

    static EntityManagerFactory entityManagerFactory;

    public static EntityManagerFactory getEntityManagerFactory()
    {
        if(entityManagerFactory==null)
        {
            entityManagerFactory= Persistence.createEntityManagerFactory("level2");
        }
        return entityManagerFactory;
    }
}
