package SERVLET;


import DAO.FactoryProvider;
import ENTITY.Flight;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/BookingServlet")
public class BookingServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");


        int userId=Integer.parseInt(req.getParameter("userId"));
        String source=req.getParameter("source");
        String destination=req.getParameter("destination");

        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<Flight> root= criteriaQuery.from(Flight.class);


        TypedQuery typedQuery= (TypedQuery) entityManager.createQuery("Select f from Flight f where f.source='"+source+"' and f.destination='"+destination+"' ORDER BY f.price");
        List<Flight> flightsBySource= typedQuery.getResultList();


        printWriter.println("<h3>THE FLIGHTS AVAILABLE IN ASECNDING ORDER OF PRICES ARE</h3><br>");
        for (Flight f:flightsBySource) {
            printWriter.println(f+"<br><br>");
        }

        printWriter.println("<br><a href='book.html'>BOOK FLIGHT HERE</a>");



    }
}