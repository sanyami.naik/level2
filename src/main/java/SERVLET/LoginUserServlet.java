package SERVLET;

import DAO.FactoryProvider;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/LoginUserServlet")
public class LoginUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        int userId=Integer.parseInt(req.getParameter("userId"));
        String userPassword=req.getParameter("userPassword");

        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<User> root= criteriaQuery.from(User.class);

        Query query= entityManager.createQuery(criteriaQuery.select(root));
        List<User> userList=query.getResultList();

        long count=userList.stream().filter(e->e.getUserId()==userId).count();
        if(count==1)

        {
            User user=entityManager.find(User.class,userId);
            if(user.getUserPassword().equals(userPassword))
            {
                printWriter.println("User logged in successfully");
                RequestDispatcher requestDispatcher= req.getRequestDispatcher("booking.html");
                requestDispatcher.include(req,resp);
            }
            else {
                printWriter.println("User Password is wrong");
                RequestDispatcher requestDispatcher= req.getRequestDispatcher("booking.html");
                requestDispatcher.include(req,resp);
            }


        }
        else
        {
            printWriter.println("User id has not been registered register first");
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            requestDispatcher.include(req,resp);
        }
    }
}
