package SERVLET;

import DAO.BookingImplementation;
import DAO.FactoryProvider;
import ENTITY.Flight;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/BookFlightUserServlet")
public class BookFlight  extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");


        int userId=Integer.parseInt(req.getParameter("userId"));
        int flightno=Integer.parseInt(req.getParameter("flightNo"));

        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        User user=entityManager.find(User.class,userId);
        Flight flight=entityManager.find(Flight.class,flightno);

        if(user!=null)
        {
            BookingImplementation bookingImplementation=new BookingImplementation();
            bookingImplementation.insertBooking(user,flight);
        }
    }
}
